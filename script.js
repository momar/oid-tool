
(function() {
	const searchString = document.location.search.replace(/^\?/, "").split("&");

	const format = (searchString.filter(x => x.match(/^format=/))[0]||"").substr(7);
	document.getElementById("oid-format").value = format || "hex";

	const relative = searchString.filter(x => x.match(/^relative(=|$)/)).length > 0;
	document.getElementById("oid-relative").checked = relative;

	const oid = (searchString.filter(x => x.match(/^oid=/))[0]||"").substr(4);
	if (oid && oid.indexOf(".") >= 0) { document.getElementById("oid-string").value = oid; convertFromString(); }
	else if (oid) { document.getElementById("oid-hex").value = oid; convertToString(); }
})();

function updateUrl(format, relative) {
	const searchParams = [].concat(format == "hex" ? [] : ["format=" + format]).concat(relative ? ["relative"] : []);
	console.log(document.location.search);
	if (document.location.search.match(/(?:\?|&)oid=/)) searchParams.push(document.location.search.match(/(?:\?|&)(oid=[^&]*)/)[1]);
	const searchString = searchParams.join("&").replace(/^(?=.)/, "?");
	if (searchString != document.location.search) history.replaceState({}, "", document.location.pathname + searchString);
}

function updateOid() {
	document.getElementById("oid-info").href = "http://oid-info.com/get/" + document.getElementById("oid-string").value;
}

function convertFromString() {
	updateOid();

	const format = document.getElementById("oid-format").value;
	const relative = document.getElementById("oid-relative").checked;
	updateUrl(format, relative);

	let input = document.getElementById("oid-string").value;
	if (relative) input = "1.3.6.1.4.1." + input;
	let output = jsrsasign.KJUR.asn1.ASN1Util.oidIntToHex(input).toLowerCase();
	if (relative) output = output.substr(10);
	if (format == "cbor" || format == "cbor-esc") {
		if (relative) output = "d86f" + output;
		else if (output.indexOf("2b06010401") === 0 && output.length > 10) output = "d870" + output.substr(10);
		else output = "d86e" + output;

		let prefix = (2 << 5);
		const size = (output.length - 4) / 2;
		if (size < 24) prefix = (prefix + size).toString(16);
		else {
			const bytes = (
				size > Math.pow(2, 3*8) - 1 ? 4 :
				size > Math.pow(2, 2*8) - 1 ? 3 :
				size > Math.pow(2, 1*8) - 1 ? 2 :
				1
			);
			prefix = (prefix + 23 + bytes).toString(16) + size.toString(16).replace(/^(?=.(..)*$)/, "0");
		}
		output = output.substr(0, 4) + prefix + output.substr(4);
	}
	if (format == "hex" || format == "cbor") output = output.replace(/(..)(?!$)/g, "$1 ");
	if (format == "esc" || format == "cbor-esc") output = output.replace(/(..)/g, "\\x$1");

	document.getElementById("oid-hex").value = output;
}

function convertToString() {
	const format = document.getElementById("oid-format").value;
	let relative = document.getElementById("oid-relative").checked;
	let revertRelative = relative;

	let input = document.getElementById("oid-hex").value.toLowerCase();
	input = input.replace(/\s+|\\x/g, "");
	if (format == "cbor" || format == "cbor-esc") {
		if (input.indexOf("d870") === 0) { relative = true; revertRelative = false; document.getElementById("oid-relative").checked = false; }
		else if (input.indexOf("d86f") === 0) { relative = true; revertRelative = true; document.getElementById("oid-relative").checked = true; }
		else if (input.indexOf("d86e") === 0) { relative = false; revertRelative = false; document.getElementById("oid-relative").checked = false; }
		updateUrl(format, relative);
		input = input.substr(4);

		if (input.substr(0, 2) == "58") input = input.substr(4);
		else if (input.substr(0, 2) == "59") input = input.substr(6);
		else if (input.substr(0, 2) == "60") input = input.substr(8);
		else if (input.substr(0, 2) == "61") input = input.substr(10);
		else input = input.substr(2);
	}
	if (relative) input = "2b06010401" + input;
	let output = jsrsasign.KJUR.asn1.ASN1Util.oidHexToInt(input);
	if (revertRelative) output = output.substr(12);

	document.getElementById("oid-string").value = output;
	updateOid();
}
